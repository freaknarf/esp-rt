/* Hello World Example
 
   This example code is in the Public Domain (or CC0 licensed, at your option.)
 
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#define BLINK_GPIO 7
#include "driver/gpio.h"
// Include Libraries(DHT)
//#include "driver/dht.h"
//dht DHT;
// Pin Definitions (DHT)
#define DHT_PIN_DATA	2 //humidity sensor
#define LEDB_PIN_VIN	14 //led pin
#define LEDG_PIN_VIN	12 //led pin
#define LEDY_PIN_VIN	15 //led pin
#define POTENTIOMETER3V3_PIN_SIG	A0 // potentionmetree //pin analogique
#define PUSHBUTTON_PIN_2	13 //button
#define SCREEN 14 // 7 led Digital display
    gpio_config_t pin7;

    /* pin for red led  */



 

   
void hello_task(void *pvParameter)
{
while(1)
	{
	    printf("Hello world!\n");
	    vTaskDelay(1000 / portTICK_RATE_MS);
	}
}
 void blink_task(void *pvParameter)
{
       pin7.mode = GPIO_MODE_OUTPUT;

    pin7.pin_bit_mask = GPIO_Pin_13;

    pin7.pull_down_en = 0;

    pin7.pull_up_en = 0;

    gpio_config(&pin7);
    while(1) {
        /* Blink off (output low) */
         gpio_set_level(GPIO_NUM_13, 0) ;
        	    printf("goodbye  world1!\n");

       vTaskDelay(1000 / portTICK_RATE_MS);
        /* Blink on (output high) */
         gpio_set_level(GPIO_NUM_13, 1) ;
                	    printf("goodbye  world2!\n");
        vTaskDelay(1000 / portTICK_RATE_MS);
    }
}
  void read_values(void *pvParameter)
{
   //code pour le capteur d'humidité
  //  int chk = DHT.read11(DHT_PIN_DATA); //lecture des valeurs du pin
    //Serial.Print('Temperature : ');
   // Serial.PrintLine(DHT.temperature);
  //  Serial.Print('Humidity : ');
   // Serial.PrintLine(DHT.humidity);
//vTaskDelay(100 / portTICK_RATE_MS);
}
/******************************************************************************
 * FunctionName : app_main
 * Description  : entry of user application, init user function here
 * Parameters   : none
 * Returns      : none
*******************************************************************************/
void app_main(void)
{
    xTaskCreate(&hello_task, "hello_task", 2048, NULL, 5, NULL);
    xTaskCreate(&blink_task, "blink_task", 2048, NULL, 5, NULL);


}
